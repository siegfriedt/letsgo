<?php

namespace App\Entity;

use App\Repository\CaseJeuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CaseJeuRepository::class)
 */
class CaseJeu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $posX;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $posY;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $valCJ;

    /**
     * @ORM\ManyToOne(targetEntity=Partie::class, inversedBy="Cases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idPartie;

    public function __construct(int $posX, int $posY)
    {
        $this->setPosX($posX);
        $this->setPosY($posY);
        $this->setValCJ(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCJ(): ?int
    {
        return $this->idCJ;
    }

    public function setIdCJ(int $idCJ): self
    {
        $this->idCJ = $idCJ;

        return $this;
    }

    public function getPosX(): ?int
    {
        return $this->posX;
    }

    public function setPosX(?int $posX): self
    {
        $this->posX = $posX;

        return $this;
    }

    public function getPosY(): ?int
    {
        return $this->posY;
    }

    public function setPosY(?int $posY): self
    {
        $this->posY = $posY;

        return $this;
    }

    public function getValCJ(): ?int
    {
        return $this->valCJ;
    }

    public function setValCJ(?int $valCJ): self
    {
        $this->valCJ = $valCJ;

        return $this;
    }

    public function getIdPartie(): ?Partie
    {
        return $this->idPartie;
    }

    public function setIdPartie(?Partie $id): self
    {
        $this->idPartie = $id;

        return $this;
    }
}
