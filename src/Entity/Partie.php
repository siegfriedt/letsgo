<?php

namespace App\Entity;

use App\Repository\PartieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PartieRepository::class)
 */
class Partie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbTours;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbPointsNoir;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbPointsBlanc;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $mdpPartie;

    /**
     * @ORM\OneToMany(targetEntity=CaseJeu::class, mappedBy="idPartie", orphanRemoval=true)
     */
    private $Cases;

    public function __construct(string $mdp)
    {
        $this->setMdpPartie($mdp);
        $this->setNbTours(1);
        $this->setNbPointsBlanc(0);
        $this->setNbPointsNoir(0);
        $this->Cases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbTours(): ?int
    {
        return $this->nbTours;
    }

    public function setNbTours(?int $nbTours): self
    {
        $this->nbTours = $nbTours;

        return $this;
    }

    public function getNbPointsNoir(): ?int
    {
        return $this->nbPointsNoir;
    }

    public function setNbPointsNoir(?int $nbPointsNoir): self
    {
        $this->nbPointsNoir = $nbPointsNoir;

        return $this;
    }

    public function getNbPointsBlanc(): ?int
    {
        return $this->nbPointsBlanc;
    }

    public function setNbPointsBlanc(?int $nbPointsBlanc): self
    {
        $this->nbPointsBlanc = $nbPointsBlanc;

        return $this;
    }

    public function getMdpPartie(): ?string
    {
        return $this->mdpPartie;
    }

    public function setMdpPartie(?string $mdpPartie): self
    {
        $this->mdpPartie = $mdpPartie;

        return $this;
    }

    /**
     * @return Collection|CaseJeu[]
     */
    public function getCases(): Collection
    {
        return $this->Cases;
    }

    public function addCase(CaseJeu $case): self
    {
        if (!$this->Cases->contains($case)) {
            $this->Cases[] = $case;
            $case->setIdPartie($this);
        }

        return $this;
    }

    public function removeCase(CaseJeu $case): self
    {
        if ($this->Cases->removeElement($case)) {
            // set the owning side to null (unless already changed)
            if ($case->getIdPartie() === $this) {
                $case->setIdPartie(null);
            }
        }

        return $this;
    }
}
