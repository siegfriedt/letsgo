<?php

namespace App\Repository;

use App\Entity\CaseJeu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CaseJeu|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaseJeu|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaseJeu[]    findAll()
 * @method CaseJeu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaseJeuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaseJeu::class);
    }

    // /**
    //  * @return CaseJeu[] Returns an array of CaseJeu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CaseJeu
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
