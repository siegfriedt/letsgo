<?php
// src/Controller/AccueilController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Partie;
use App\Entity\CaseJeu;
use Doctrine\ORM\EntityManagerInterface;


class AccueilController extends AbstractController
{
    /**
      * @Route("/", name="root")
      */
    public function Root()
    {
        return $this->redirectToRoute('accueil');
    }

    /** 
      * @Route("/Accueil", name="accueil")
      */
    public function PageAccueil()
    {
        return $this->render('accueil.html.twig', [
          "page" => "accueil"
        ]);
    }

    /**
      * @Route("/Contact", name="contact")
      */
    public function PageContact()
    {
        return $this->render('contact.html.twig', [
          "page" => "contact"
        ]);
    }

    /**
      * @Route("/CV", name="CV")
      */
    public function getCV()
    {
      return new BinaryFileResponse("cv/CV_Romain_Siegfriedt.pdf");
    }

    /**
      * @Route("/Jouer", name="jouer")
      */
    public function PageJouer()
    {
        return $this->render('jouer.html.twig', [
          "page" => "jouer"
        ]);
    }

    /**
      * @Route("/CreerPartie", name="creerPartie")
      */
      public function CreerPartie()
      {
        $request = Request::createFromGlobals();
        $mdp = $request->request->get('mdp', 'default mdp');

        $entityManager = $this->getDoctrine()->getManager();  
        $partie = new Partie($mdp);
        $entityManager->persist($partie);

        foreach (range(0, 11) as $x) {
          foreach (range(0, 11) as $y) {
              $case = new CaseJeu($x, $y);
              $entityManager->persist($case);
              $partie->addCase($case);
          }
        }

        $entityManager->flush();

        return $this->render('jeu.html.twig', [
          "page" => "jouer",
          "couleur" => "blanc"
        ]);
      }

    /**
      * @Route("/jeu", name="recharge")
      */
      public function RechargeJeu()
      {
        $request = Request::createFromGlobals();
        $couleur = $request->request->get('couleur', 'spectateur');

          return $this->render('jeu.html.twig', [
            "page" => "jouer",
            "couleur" => $couleur
          ]);
      }

}