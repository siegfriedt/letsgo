<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210802170528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE case_jeu (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, id_partie_id INTEGER NOT NULL, id_cj INTEGER NOT NULL, pos_x INTEGER DEFAULT NULL, pos_y INTEGER DEFAULT NULL, val_cj INTEGER DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_2428F36160404B83 ON case_jeu (id_partie_id)');
        $this->addSql('CREATE TABLE partie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, id_partie INTEGER NOT NULL, nb_tours INTEGER DEFAULT NULL, nb_points_noir INTEGER DEFAULT NULL, nb_points_blanc INTEGER DEFAULT NULL, mdp_partie VARCHAR(30) DEFAULT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE case_jeu');
        $this->addSql('DROP TABLE partie');
    }
}
