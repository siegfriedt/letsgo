<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210811164357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2428F36160404B83');
        $this->addSql('CREATE TEMPORARY TABLE __temp__case_jeu AS SELECT id, id_partie_id, pos_x, pos_y, val_cj FROM case_jeu');
        $this->addSql('DROP TABLE case_jeu');
        $this->addSql('CREATE TABLE case_jeu (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, id_partie_id INTEGER NOT NULL, pos_x INTEGER DEFAULT NULL, pos_y INTEGER DEFAULT NULL, val_cj INTEGER DEFAULT NULL, CONSTRAINT FK_2428F36160404B83 FOREIGN KEY (id_partie_id) REFERENCES partie (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO case_jeu (id, id_partie_id, pos_x, pos_y, val_cj) SELECT id, id_partie_id, pos_x, pos_y, val_cj FROM __temp__case_jeu');
        $this->addSql('DROP TABLE __temp__case_jeu');
        $this->addSql('CREATE INDEX IDX_2428F36160404B83 ON case_jeu (id_partie_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__partie AS SELECT nb_tours, nb_points_noir, nb_points_blanc, mdp_partie, id FROM partie');
        $this->addSql('DROP TABLE partie');
        $this->addSql('CREATE TABLE partie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nb_tours INTEGER DEFAULT NULL, nb_points_noir INTEGER DEFAULT NULL, nb_points_blanc INTEGER DEFAULT NULL, mdp_partie VARCHAR(30) DEFAULT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO partie (nb_tours, nb_points_noir, nb_points_blanc, mdp_partie, id) SELECT nb_tours, nb_points_noir, nb_points_blanc, mdp_partie, id FROM __temp__partie');
        $this->addSql('DROP TABLE __temp__partie');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2428F36160404B83');
        $this->addSql('CREATE TEMPORARY TABLE __temp__case_jeu AS SELECT id, id_partie_id, pos_x, pos_y, val_cj FROM case_jeu');
        $this->addSql('DROP TABLE case_jeu');
        $this->addSql('CREATE TABLE case_jeu (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, id_partie_id INTEGER NOT NULL, pos_x INTEGER DEFAULT NULL, pos_y INTEGER DEFAULT NULL, val_cj INTEGER DEFAULT NULL, id_cj INTEGER NOT NULL)');
        $this->addSql('INSERT INTO case_jeu (id, id_partie_id, pos_x, pos_y, val_cj) SELECT id, id_partie_id, pos_x, pos_y, val_cj FROM __temp__case_jeu');
        $this->addSql('DROP TABLE __temp__case_jeu');
        $this->addSql('CREATE INDEX IDX_2428F36160404B83 ON case_jeu (id_partie_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__partie AS SELECT id, nb_tours, nb_points_noir, nb_points_blanc, mdp_partie FROM partie');
        $this->addSql('DROP TABLE partie');
        $this->addSql('CREATE TABLE partie (id_partie INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nb_tours INTEGER DEFAULT NULL, nb_points_noir INTEGER DEFAULT NULL, nb_points_blanc INTEGER DEFAULT NULL, mdp_partie VARCHAR(30) DEFAULT NULL, id INTEGER NOT NULL)');
        $this->addSql('INSERT INTO partie (id, nb_tours, nb_points_noir, nb_points_blanc, mdp_partie) SELECT id, nb_tours, nb_points_noir, nb_points_blanc, mdp_partie FROM __temp__partie');
        $this->addSql('DROP TABLE __temp__partie');
    }
}
